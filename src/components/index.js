import About from './About/About';
import CollectionDetail from './CollectionDetail/CollectionDetail';
import Collections from './Collections/Collections';
import Home from './Home/Home';
import Nav from './Nav/Nav';
import NotFound from './NotFound/NotFound';
import PhotoDetail from './PhotoDetail/PhotoDetail';
import Photos from './Photos/Photos';
import PhotoStatistics from './PhotoStatistics/PhotoStatistics';
import SearchBar from './SearchBar/SearchBar';
import Categories from './Categories/Categories';
import UserProfile from './UserProfile/UserProfile';

export {
    About,
    CollectionDetail,
    Collections,
    Home,
    Nav,
    NotFound,
    PhotoDetail,
    Photos,
    PhotoStatistics,
    SearchBar,
    Categories,
    UserProfile
}